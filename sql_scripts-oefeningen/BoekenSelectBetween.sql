--BV
--16/04/2018

--oef1
use ModernWays;
select Voornaam,
Familienaam,
Titel
from Boeken
where Verschijningsjaar between '1998' and '2001'; 

--oef2
use ModernWays;
select Voornaam,
Familienaam,
Titel
from Boeken
where Verschijningsjaar between '1900' and '2011'; 
and Familienaam like 'B%'
order by Familienaam asc;

--oef3
use ModernWays;
select Voornaam,
Familienaam,
Titel,
from Boeken
where Verschijningsjaar between '1900' and '2011'; 
and Familienaam like 'B%' or
and Familienaam like 'F%' or
and Familienaam like 'A%'
order by Verschijningsjaar asc;

--oef4
use ModernWays;
select Voornaam,
Familienaam,
Titel,
from Boeken
where Verschijningsjaar between '1990' and '2019'; 
order by Verschijningsjaar asc;

--oef5
use ModernWays;
select Voornaam,
Familienaam,
Titel,
Verschijningsjaar,
from Boeken
where Verschijningsjaar between '0' and '2009'; 
order by Familienaam asc,
Voornaam asc,