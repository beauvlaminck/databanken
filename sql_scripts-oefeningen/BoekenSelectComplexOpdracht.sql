--BV
--16/04/2018
--les 8 afstands opdrachten 

-- oef.1
use ModernWays;
select Familienaam,Titel,Verschijningsjaar
from Boeken
where not(Familienaam like'D%' or Familienaam like 'K' or Familienaam like 'R%')
and (verschijningsjaar like '192_' or Verschijningsjaar like '199_');

--oef2
use ModernWays;
select Familienaam,Titel,Verschijningsjaar
from Boeken
where(Familienaam like'D%' or Familienaam like 'K' or Familienaam like 'R%')
and (verschijningsjaar like '19_2');
