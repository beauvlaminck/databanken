--BV
--16-04-2018

--oef1
use ModernWays;
select distinct Familienaam,Voornaam
from Boeken
   order by Familienaam asc,Voornaam;
   
   --oef 2
   use ModernWays;
   select distinct Familienaam,
   Voornaam
from Boeken
Where(Familienaam like '%a%'or Familienaam like '%e%')
   order by Familienaam asc, Voornaam;