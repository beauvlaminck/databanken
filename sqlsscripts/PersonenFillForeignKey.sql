--BV
--23/05/2018

use ModernWays;
update Personen, Aanspreektitel
	set IdAanspreektitel = 
		(select Aanspreektitel.Id 
         from Aanspreektitel 
         where Aanspreektitel.Description = Personen.AanspreekTitel);
