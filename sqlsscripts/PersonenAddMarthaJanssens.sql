use ModernWays;
insert into Personen (
    Voornaam,
    Familienaam,
    Aanspreektitel,
    Straat,
    Stad,
    Huisnummer,
    Biografie,
    Commentaar)
values (
    'Martha',
    'Janssens',
    'Mevrouw',
    'Plopperdeplopstraat',
    'Mechelen',
    '23',
    'Ze leefde lang en gelukkig.',
    'Goede schrijver'
    )
    