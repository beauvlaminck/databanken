-- Selectie van 2 boeken van twee verschillende auteurs 
use ModernWays;
select Familienaam, Titel from Boeken
   where Familienaam = 'Braudel' or Familienaam = 'Gowers';

-- Selectie van alle boeken buiten van 1 auteur   
use ModernWays;
select Familienaam, Titel from Boeken
   where not Familienaam = 'Braudel';
   
-- 3 nieuwe rijen inserten
use ModernWays;
insert into Boeken (Voornaam, Familienaam, Titel, Stad, Uitgeverij, Verschijningsjaar)
    values ('Hugo', 'Claus', 'De verwondering', 'Antwerpen', 'Manteau', '1970'),
			('Hugo', 'Raes', 'Jagen en gejaagd worden', 'Antwerpen', 'De Bezige Bij', '1954'),
			('Jean-Paul', 'Sarthe', 'Het zijn en het niets', '1943', 'Parijs', 'Gallimard');


--Selecteer alle boeken van de auteurs die de voornaam Hugo of Jean-Paul hebben
use ModernWays;
select Voornaam, Familienaam, Titel from Boeken
    where Voornaam = 'Hugo' or Voornaam = 'Jean-Paul';


Selecteer alle boeken van de auteurs die de voornaam Hugo of Jean-Paul hebben en een boek in 1970 hebben geschreven;
use ModernWays;
select Voornaam, Familienaam, Titel, Verschijningsjaar from Boeken
    where (Voornaam = 'Hugo' or Voornaam = 'Jean-Paul') 
    and Verschijningsjaar = '1970';


--Sarthe verkeerd geschreven het moet Sartre zijn, schrijf een update statement die die familienaam verbetert;
use ModernWays;
update Boeken
   set Familienaam = 'Sartre'
   where Familienaam = 'Sarthe';



--Voeg de categorie 'Literatuur' toe voor de boeken van Hugo Claus en Hugo Raes.
Use ModernWays;
update Boeken
   set Categorie = 'Literatuur'
   where (Familienaam = 'Claus' and Voornaam = 'Hugo') 
   or (Familienaam = 'Raes' and Voornaam = 'Hugo');


--Voor het boek van Jean-Paul Sartre met de titel Het zijn en het niets voeg je de categorie 'Filosofie' to
Use ModernWays;
update Boeken
   set Categorie = 'Filosofie'
   where (Familienaam = 'Sartre' and Voornaam = 'Jean-Paul' and Titel = 'Het zijn en het niets');
   

-- Selecteer de boeken van Sartre met de categorie Filosofie en de boeken geschreven door Hugo Claus.  
use ModernWays;
select Voornaam, Familienaam, Titel, Categorie, Verschijningsjaar from Boeken
    where (Familienaam = 'Sartre' and Categorie = 'Filosofie') 
    or (Voornaam = 'Hugo' and Familienaam = 'Claus');
   
